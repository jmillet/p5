import argparse
import datetime
import re
from pymongo import MongoClient

def validate_language_code(arg_value, pat=re.compile(r"^[a-z]{2}$")):
    if not pat.match(arg_value):
        raise argparse.ArgumentTypeError
    return arg_value

def parse_arguments():
    parser = argparse.ArgumentParser(
        description=f"Query mongodb twitter database for hashtags",
        epilog=""
    )

    parser.add_argument('-l', '--language', type=validate_language_code)
    
    parser.add_argument("-f", "--from",
                        type=lambda s: datetime.datetime.strptime(s, '%Y-%m-%dT%H'),
                        help="")
    
    parser.add_argument("-t", "--to",
                        type=lambda s: datetime.datetime.strptime(s, '%Y-%m-%dT%H'),
                        help=""
                        )
    
    args = vars(parser.parse_args())
    if bool(args['from']) ^ bool(args['to']):
        parser.error('--from and --to must be given together.')
    
    date_from = args['from']
    date_to = args['to']
    language = args['language']
    if date_from > date_to:
        print('Date "from": {} cannot be posterior to date "to": {}'.format(date_from, date_to))
        exit(0)
    
    return [language, date_from, date_to]


[_lang, _from, _to] = parse_arguments()

client = MongoClient(
    'mongodb://root:example@localhost:27017/twitter?authSource=admin&ssl=false&ssl_cert_reqs=CERT_NONE')

r = list(client.get_database("twitter").get_collection('active_views').find({'active': True}))
active_view = r[0]['view_name']
print("Looking up hastags in language '{}' between {} and {} (speed layer table is {}) ...".format(_lang, _from, _to, active_view))

htags = list(client.get_database("twitter").get_collection('htags').aggregate([
    {'$unionWith': {'coll': active_view}},
    {'$match':
        {'date':
            {
                '$gte': _from,
                '$lt': _to
            },
            'lang': _lang
        }
    },
    {"$group": {
        "_id": {
            "tag": "$tag"
        },
        "total_count": {"$sum": "$count"}
    }},
    {"$sort": {"total_count": -1}},
    {"$limit": 10}
]))
for tags in htags:
    print("{} {}".format(tags['_id']['tag'], str(tags['total_count'])))
