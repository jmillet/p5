docker exec -t spark-master \
/bin/bash -c "/opt/bitnami/spark/bin/spark-submit \
--driver-memory=1g \
--conf 'spark.executor.memory=2g' \
--master local[2] \
--conf 'spark.executor.extraJavaOptions=-XX:+UseG1GC' \
--packages org.apache.spark:spark-avro_2.12:3.0.2,org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.2  \
/var/code/python/batch_layer/serialize_tweets.py"
