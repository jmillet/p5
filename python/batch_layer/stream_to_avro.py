from datetime import datetime
import json
import os

import fastavro
from hdfs import InsecureClient
from kazoo.client import KazooClient  # Run `pip install kazoo` to install this package
from pyspark.sql import SparkSession
from pyspark.sql.functions import SparkContext

hdfs_client = InsecureClient('http://namenode:14000', user='hue')
hdfs_ipc = "hdfs://namenode:9000"

output_path_prefix = hdfs_ipc + "/etl/input/org.jmillet.twitter/tweets/"

with hdfs_client.read(hdfs_path="/metadata/avro/schemas/org.jmillet.twitter/tweet.avsc",
                      encoding='UTF-8') as schema_file:
    schema = fastavro \
        .schema \
        .parse_schema(json.loads(schema_file.read()))


def get_kafka_broker(zk, path):
    zk = KazooClient(hosts=zk, read_only=True)
    zk.start()
    for node in zk.get_children(path + '/brokers/ids'):
        data, stats = zk.get(path + '/brokers/ids/' + node)
        props = json.loads(data)
        yield props['host'] + ':' + str(props['port'])
    zk.stop()


brokers = get_kafka_broker('zookeeper:2181', '')
broker = next(brokers)

sc = SparkContext()
spark = SparkSession\
    .builder\
    .getOrCreate()

sc.setJobDescription("Twitter streaming to avro serialization.")

stream = spark \
    .readStream \
    .format("kafka") \
    .option("kafka.bootstrap.servers", broker) \
    .option("subscribe", "tweets") \
    .option("failOnDataLoss", False) \
    .load()

def foreach_batch_function(df, epoch_id):
    """
    Serialize batch DF to HDFS Avro files.
    :param df:
    :param epoch_id:
    :return:
    """
    print("Serializing avro files in {} ...".format(datetime.now().strftime('%Y/%m/%d/%H/%M')))
    df.write \
        .option("forceSchema", schema) \
        .format('avro') \
        .save(os.path.join(output_path_prefix, datetime.now().strftime('%Y/%m/%d/%H/%M')))


tweets = stream \
    .dropDuplicates() \
    .repartition(1) \
    .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)") \
    .writeStream \
    .option("checkpointLocation", os.path.join(output_path_prefix, "checkpoint_stream_to_avro")) \
    .option("failOnDataLoss", False) \
    .outputMode("append") \
    .foreachBatch(foreach_batch_function) \
    .trigger(processingTime='60 seconds') \
    .start()

tweets.awaitTermination()
