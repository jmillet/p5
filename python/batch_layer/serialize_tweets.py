import argparse
import datetime
import hashlib
import json
import os
from argparse import RawTextHelpFormatter

import fastavro
from hdfs import InsecureClient
from pymongo import MongoClient
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.functions import SparkContext
from pyspark.sql.functions import col

"""
For current docker images docker.io/bitnami/spark stakes we are restricted to :
- python 3.6 (can be upgraded to 3.7 but is it worth anything down there ?)
- spark 3.0.2
"""


def parse_arguments():
    parser = argparse.ArgumentParser(
        description=f"Extracts Hashtags for a given hour from serialized {input_path_prefix} avro files. \n"
                    "Process last hour if no arguments are given. \n",
        epilog="Mind that this job only publish data concerning a given hour ! \n\n"
               "Last hour by default: will process received avro files last hour  \n"
               "--hour if specified : avro files received this hour will be processed. \n"
               "Using a --from --to period: avro files received between this time interval will be processed. "
               "Only data matching --hour parameter will be published. "
               "Use with caution !\n\n"
               "15mn intervals are added on both period sides for files that are processed:\n"
               " - added for late data\n"
               " - substracted for clock sync issues \n"
        , formatter_class=RawTextHelpFormatter
    )
    
    parser.add_argument("-f", "--from",
                        type=lambda s: datetime.datetime.strptime(s, '%Y-%m-%dT%H'),
                        help="Process avro files received from 'date hour' as ISO format YYYY-MM-DDTHH. \n"
                             "Used for catching up with operating problems in case avro files are received lately."
                             "Requires --hour to be defined in order to discard any other data.")
    
    parser.add_argument("-t", "--to",
                        type=lambda s: datetime.datetime.strptime(s, '%Y-%m-%dT%H'),
                        help="Process avro files received up to 'date hour' as ISO format YYYY-MM-DDTHH \n"
                             "Used for catching up with operating problems in case avro files are received lately."
                             "Requires --hour to be defined in order to discard any other data."
                        )
    
    parser.add_argument("-H", "--hour",
                        type=lambda s: datetime.datetime.strptime(s, '%Y-%m-%dT%H'),
                        help="Explicitly process avro files this hour (ISO format YYYY-MM-DDTHH). \n"
                             "This parameter defines the batch hour ! "
                             "i.e. this is the only data that will get published.")
    
    args = vars(parser.parse_args())
    if bool(args['from']) ^ bool(args['to']):
        parser.error('--from and --to must be given together.')
    
    if bool(args['from']) ^ bool(args['hour']):
        parser.error('Defining a (from, to) period requires a batch --hour to be defined.')
    
    date_from = args['from']
    date_to = args['to']
    hour = args['hour']
    
    if hour is None:
        # process last hour files, batch hour is last hour
        date_from = (datetime.datetime.now() - datetime.timedelta(hours=1)).replace(minute=0, second=0, microsecond=0)
        date_to = (datetime.datetime.now()).replace(minute=0, second=0, microsecond=0)
        hour = date_from
    elif date_from is None:
        # process "hour" files, batch hour is "--hour"
        date_from = hour
        date_to = (hour + datetime.timedelta(hours=1)).replace(minute=0, second=0, microsecond=0)
    else:
        # process from/to files, batch hour is "--hour"
        # --from --to need to be coherent
        pass
    
    if date_from > date_to:
        print('Date "from": {} cannot be posterior to date "to": {}'.format(date_from, date_to))
        exit(0)
    
    return [hour, date_from, date_to]


def avro_file_paths_from_time_range(start, stop):
    start = start - datetime.timedelta(minutes=15)
    stop = stop + datetime.timedelta(minutes=15)
    return [input_path_prefix + (start + datetime.timedelta(hours=i / 60)).strftime('%Y/%m/%d/%H/%M/*.avro')
            for i in range(int((stop - start).total_seconds() / 60))]


def file_exists(path):
    if hdfs_client.content(path.replace(hdfs_ipc, '').replace('*.avro', ''), strict=False):
        return True
    return False


def filter_paths(pathes):
    return [p for p in pathes if (file_exists(p + '_SUCCESS'))]


def extract_hashtags(tweet_data):
    json_tweet_data = json.loads(tweet_data['value'].strip())
    return [{'lang': json_tweet_data['lang'], 'created_at': json_tweet_data['created_at'], 'tag': m['tag']}
            for m in json_tweet_data['hashtags']]


def persist(df):
    client = MongoClient(
        'mongodb://root:example@mongo:27017/twitter?authSource=admin&ssl=false&ssl_cert_reqs=CERT_NONE')
    htags = client.twitter.htags
    with client.start_session() as session:
        with session.start_transaction():
            for row in df:
                htags.update_one({"_id": str(
                    hashlib.md5("{}-{}-{}".format(str(row['hour']), row['lang'], row['tag']).encode('UTF-8')))},
                    {"$set":
                         {'date': row['hour'], 'lang': row['lang'], 'tag': row['tag'],
                          'count': row['occurences']}
                     },
                    upsert=True)
            active_view = client.twitter.active_views.find_one({"active": True})
            unactive_view = client.twitter.active_views.find_one({"active": False})
            d = client.twitter[unactive_view['view_name']].delete_many(
                {"date": {"$lt": _hour + datetime.timedelta(hours=1)}})
            client.twitter.active_views.update_one({"_id": active_view['_id']}, {"$set": {'active': False}})
            client.twitter.active_views.update_one({"_id": unactive_view['_id']}, {"$set": {'active': True}})
            print(d.deleted_count, " documents deleted !!")


hdfs_client = InsecureClient('http://namenode:14000', user='hue')
hdfs_ipc = "hdfs://namenode:9000"
input_path_prefix = hdfs_ipc + "/etl/input/org.jmillet.twitter/tweets/"
output_path_prefix = hdfs_ipc + "/etl/output/org.jmillet.twitter/hourly_tag_counts/"

[_hour, _from, _to] = parse_arguments()
print('Batch Hour : {}. Seeking files {} from: {}, to: {} ...'.format(_hour, input_path_prefix, _from, _to))

with hdfs_client.read(hdfs_path="/metadata/avro/schemas/org.jmillet.twitter/hourly_tag_counts.avsc",
                      encoding='UTF-8') as schema_file:
    schema = fastavro \
        .schema \
        .parse_schema(json.loads(schema_file.read()))

sc = SparkContext()
spark = SparkSession.builder.getOrCreate()
sc.setJobDescription("org.jmillet.tweeter hourly tag counters serialization")
# p = avro_file_paths_from_time_range(_from, _to)
# print(p)
# f = filter_pathes(p)
# print(f)

tweets = spark.read.format("avro") \
    .load(filter_paths(avro_file_paths_from_time_range(_from, _to)))

# tweets.show(truncate=False, )

hashtags = tweets.rdd.flatMap(extract_hashtags).toDF()

# hashtags.show(truncate=False)

fmt_hour = _hour.strftime('%Y-%m-%dT%H:%M:%S')
hashtag_counts = hashtags \
    .select(F.date_trunc("Hour", "created_at").alias("hour"), "lang", "tag") \
    .filter(col("hour") == fmt_hour) \
    .groupBy("hour", "lang", "tag") \
    .count() \
    .select("hour", "lang", "tag", col("count").alias("occurences"))

# hashtag_counts.show(truncate=False)

# Serialize to keep track of executions
t = hashtag_counts.repartition(1).write \
    .option("forceSchema", schema) \
    .format('avro') \
    .mode("overwrite") \
    .save(os.path.join(output_path_prefix + _hour.strftime('%Y-%m-%dT%H')))

persist(hashtag_counts.collect())
# TODO publish
# start trx
# push data
# delete from future view
# switch view
# commit
