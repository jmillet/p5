#! /usr/bin/env python

# Connect to a websocket powered by blockchain.info and print events in
# the terminal in real time.
import re

invalid_escape = re.compile(r'\\[0-7]{1,3}')  # up to 3 digits for byte values up to FF

def replace_with_byte(match):
    return chr(int(match.group(0)[1:], 8))

def repair(brokenjson):
    return invalid_escape.sub(replace_with_byte, brokenjson)

import json
import sys
from datetime import datetime
import time
from kafka import KafkaProducer  # Run `pip install kafka-python` to install this package
from kazoo.client import KazooClient  # Run `pip install kazoo` to install this package


def get_kafka_producer(zookeeper_host):
    print("Connecting to zookeeper host : {}".format(zookeeper_host))
    brokers = get_kafka_broker(zookeeper_host, '')
    broker = next(brokers)
    print("Found kafka broker : {}".format(broker))
    producer = KafkaProducer(bootstrap_servers=broker)
    
    return producer


def get_kafka_broker(zk, path):
    zk = KazooClient(hosts=zk, read_only=True)
    zk.start()
    for node in zk.get_children(path + '/brokers/ids'):
        data, stats = zk.get(path + '/brokers/ids/' + node)
        props = json.loads(data)
        yield props['host'] + ':' + str(props['port'])
    zk.stop()


producer = get_kafka_producer('zookeeper:2181')

def main():
    with open('../../sample/sample_raw.txt', 'r', encoding='utf-8') as f:
    #with open('../../sample/SOJUNGHWAN.txt', 'r', encoding='utf-8') as f:
        cnt = 1
        for line in f.readlines():
            #print("Line {}: {}".format(cnt, line))
            json_response = json.loads(line.strip())
            #print(json.dumps(json_response, indent=4, sort_keys=True, ensure_ascii=False))
            #if 'tag' not in json_response['hashtags']:
            #    continue
            cnt += 1
            msg = json.dumps(
                {'lang': json_response['lang'],
                 'created_at': json_response['created_at'],
                 'id': json_response['id'],
                 'text': json_response['text'],
                 'hashtags': json_response['hashtags']})
            print(msg)
            producer.send("tweets", msg.encode(),
                          key=str(json_response['id']).encode())
            if (cnt % 100) == 0:
                time.sleep(1)
            



if __name__ == "__main__":
    main()