import json
import os

import requests
import json
import sys
from datetime import datetime
import time

import urllib3
from kafka import KafkaProducer  # Run `pip install kafka-python` to install this package
from kazoo.client import KazooClient  # Run `pip install kazoo` to install this package


# To set your environment variables in your terminal run the following line:
# export 'BEARER_TOKEN'='<your_bearer_token>'


def auth():
    return os.environ.get("BEARER_TOKEN")


def create_url():
    return "https://api.twitter.com/2/tweets/sample/stream?tweet.fields=lang,created_at,entities"


def create_headers(bearer_token):
    headers = {"Authorization": "Bearer {}".format(bearer_token)}
    return headers


def connect_to_endpoint(url, headers):
    response = requests.request("GET", url, headers=headers, stream=True)
    #print(url)
    if response.status_code != 200:
        raise Exception(
            "Request returned an error: {} {}".format(
                response.status_code, response.text
            )
        )
    empty_lines = 0
    print(response)
    for response_line in response.iter_lines():
        
        print(response_line)
        
        if not response_line:
            empty_lines += 1
            if empty_lines % 1000 == 0:
                raise Exception(
                    "Incorrect response line. Raising exception for restart."
                )
            continue
        
        json_response = json.loads(response_line.strip())
        if not 'data' in json_response:
            raise Exception(
                "Incorrect response line. Raising exception for restart."
            )
        hashtags = []
        if 'entities' in json_response['data'] and 'hashtags' in json_response['data']['entities']:
            hashtags = json_response['data']['entities']['hashtags']
    
        msg = json.dumps(
            {'lang': json_response['data']['lang'],
             'created_at': json_response['data']['created_at'],
             'id': json_response['data']['id'],
             'text': json_response['data']['text'],
             'hashtags': hashtags}
        )
        print(msg)
        producer.send("tweets", msg.encode(encoding='ascii'),
                      key=str(json_response['data']['id']).encode())
    

def get_kafka_producer(zookeeper_host):
    print("Connecting to zookeeper host : {}".format(zookeeper_host))
    brokers = get_kafka_broker(zookeeper_host, '')
    broker = next(brokers)
    print("Found kafka broker : {}".format(broker))
    producer = KafkaProducer(bootstrap_servers=broker)
    
    return producer


def get_kafka_broker(zk, path):
    zk = KazooClient(hosts=zk, read_only=True)
    zk.start()
    for node in zk.get_children(path + '/brokers/ids'):
        data, stats = zk.get(path + '/brokers/ids/' + node)
        props = json.loads(data)
        yield props['host'] + ':' + str(props['port'])
    zk.stop()


producer = get_kafka_producer('zookeeper:2181')


def main():
    bearer_token = auth()
    url = create_url()
    headers = create_headers(bearer_token)
    timeout = 0
    while True:
        try:
            connect_to_endpoint(url, headers)
            timeout += 1
        except (Exception, ConnectionResetError, urllib3.exceptions.ProtocolError, requests.exceptions.ChunkedEncodingError):
            
            continue
    

if __name__ == "__main__":
    main()
