start-stop-daemon --start --oknodo --user root --name tweet_sample \
                   --pidfile /run/tweets.pid --startas /usr/bin/docker \
                   --chuid root -- run -t --rm --net docker_app-tier \
                   --name publish-tweets \
                   -v "/home/joseph/Documents/OpenClassroom/P5/dev/:/usr/src/dev" \
                   -e BEARER_TOKEN=$(cat /home/joseph/Documents/OpenClassroom/P5/dev/auth/bearer_token.txt) \
                   docker_python python /usr/src/dev/python/publishers/get_sampled_stream.py