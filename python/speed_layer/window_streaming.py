from datetime import datetime
import json
import os

from hdfs import InsecureClient
from kazoo.client import KazooClient  # Run `pip install kazoo` to install this package
from pyspark.sql import SparkSession
from pyspark.sql.functions import SparkContext
from pyspark.sql import window
from pyspark.sql import functions as F
from pyspark.sql.types import *


def get_kafka_broker(zk, path):
    zk = KazooClient(hosts=zk, read_only=True)
    zk.start()
    for node in zk.get_children(path + '/brokers/ids'):
        data, stats = zk.get(path + '/brokers/ids/' + node)
        props = json.loads(data)
        yield props['host'] + ':' + str(props['port'])
    zk.stop()


brokers = get_kafka_broker('zookeeper:2181', '')
broker = next(brokers)

sc = SparkContext()
spark = SparkSession \
    .builder \
    .getOrCreate()

hdfs_ipc = "hdfs://namenode:9000"
output_path_prefix = hdfs_ipc + "/etl/input/org.jmillet.twitter/tweets/"

sc.setJobDescription("Speed Layer for Twitter hashtags.")

stream = spark \
    .readStream \
    .format("kafka") \
    .option("kafka.bootstrap.servers", broker) \
    .option("subscribe", "tweets") \
    .option("failOnDataLoss", False) \
    .load()

def foreach_batch_function(df, epoch_id):
    df.persist()
    df.write \
        .format('mongo') \
        .mode("append")\
        .option("database", "twitter")\
        .option("collection", "hashtags_speed_1") \
        .save()
    df.write \
        .format('mongo') \
        .mode("append") \
        .option("database", "twitter") \
        .option("collection", "hashtags_speed_2") \
        .save()
    df.unpersist()

tags = stream \
    .dropDuplicates() \
    .selectExpr("CAST(value AS STRING)").alias("tweet_data") \
    .select(
    F.get_json_object("tweet_data.value", "$.lang").alias("lang"),
    F.date_trunc("Minute", F.get_json_object("tweet_data.value", "$.created_at")).alias("minute"),
    F.explode(
        F.split(F.regexp_replace(F.get_json_object("tweet_data.value", "$.hashtags[*].tag"), '[\\[|\\]|"]', ""),
                ",")).alias(
        "tag")
) \
    .withWatermark("minute", "1 minute") \
    .groupBy(
    F.window("minute", "1 minute", "1 minute"),
    "lang",
    "tag"
) \
    .count() \
    .select(F.col("window.start").alias("date"), F.hash(F.concat("window.start", "lang", "tag")).alias("_id"), F.col("lang"),
            F.col("tag"), F.col("count")) \
    .writeStream \
    .foreachBatch(foreach_batch_function) \
    .option("checkpointLocation", os.path.join(output_path_prefix, "checkpoint_speed_layer")) \
    .outputMode("update") \
    .start()

tags.awaitTermination()
