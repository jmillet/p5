--packages org.mongodb.spark:mongo-spark-connector_2.12:3.0.1
# start httpfs on namenode
sudo docker exec -it namenode /bin/bash
hdfs --daemon start httpfs
/opt/hadoop-3.2.1/bin/hdfs dfs -mkdir -p /etl/input/org.jmillet.twitter/tweets
/opt/hadoop-3.2.1/bin/hdfs dfs -mkdir -p /etl/output/org.jmillet.twitter/hourly_tag_counts
/opt/hadoop-3.2.1/bin/hdfs dfs -mkdir -p /metadata/avro/schemas/org.jmillet.twitter/
/opt/hadoop-3.2.1/bin/hdfs dfs -copyFromLocal /hadoop/dfs/hdfs/metadata/avro/schemas/org.jmillet.twitter/*.avsc /metadata/avro/schemas/org.jmillet.twitter/

# in spark master
sudo docker exec -it spark-master /bin/bash
cd /opt/bitnami/spark/
```bash
bin/pyspark --packages org.mongodb.spark:mongo-spark-connector_2.12:3.0.1     \
--conf "spark.mongodb.input.uri=mongodb://root:example@mongo/twitter.toto/?authSource=admin"     \
--conf "spark.mongodb.output.uri=mongodb://root:example@mongo/twitter.toto/?authSource=admin" \
--conf "spark.mongodb.input.collection=toto"    \
--conf "spark.mongodb.input.database=twitter" 
```
    
df = spark.read.format("mongo").load()    

# start httpfs on namenode
```bash
sudo docker exec -it namenode /bin/bash
hdfs --daemon start httpfs
```
# run publishers
# requires a /auth/bearer_token.txt file at project root dir
```bash
cd /home/joseph/Documents/OpenClassroom/P5/dev && \
sudo docker run -it --rm --net docker_app-tier --name publish-tweets -v "/home/joseph/Documents/OpenClassroom/P5/dev/:/usr/src/dev" -e BEARER_TOKEN=$(cat ./auth/bearer_token.txt) docker_python python /usr/src/dev/python/publishers/get_sampled_stream.py
```
# mock publishers
```bash
cd /home/joseph/Documents/OpenClassroom/P5/dev && \
sudo docker run -it --rm --net docker_app-tier --name mock-tweets -v "/home/joseph/Documents/OpenClassroom/P5/dev/:/usr/src/dev" docker_python /bin/bash -c "cd /usr/src/dev/python/publishers/ && python mock_twitter_publisher.py"
```

# clean queues
cd docker/containers && sudo rm -rf kafka*/* zookeeper_data/*

# 

sudo docker exec -it spark-master /bin/bash \
-c "/opt/bitnami/spark/bin/spark-submit --master local[*] --packages org.apache.spark:spark-avro_2.12:3.1.1,org.apache.spark:spark-sql-kafka-0-10_2.12:3.1.1  /var/code/python/batch_layer/stream_to_avro.py"

sudo docker exec -it spark-master /bin/bash \
-c "/opt/bitnami/spark/bin/spark-submit \
--conf spark.cleaner.referenceTracking.cleanCheckpoints=true \
--driver-java-options "-Dlog4j.configuration=file:///var/code/python/batch_layer/log4j.properties" \
--master local[*] \
--packages org.apache.spark:spark-avro_2.12:3.0.2,org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.2  \
/var/code/python/batch_layer/stream_to_avro.py"

# Delete topic
root@kafka:/opt/bitnami/kafka# export JMX_PORT=9998
root@kafka:/opt/bitnami/kafka# bin/kafka-topics.sh --zookeeper zookeeper:2181 --delete --topic tweets

# speed layer 
sudo docker exec -e PYTHONIOENCODING=utf8 -it spark-master /bin/bash -c "/opt/bitnami/spark/bin/spark-submit \
--conf spark.cleaner.referenceTracking.cleanCheckpoints=true \
--driver-java-options "-Dlog4j.configuration=file:///var/code/python/batch_layer/log4j.properties" \
--master local[*] \
--packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.2  \
/var/code/python/speed_layer/window_streaming.py" | grep SOJUNGHWAN


sudo docker exec -e PYTHONIOENCODING=utf8 -it spark2-master /bin/bash -c "/opt/bitnami/spark/bin/spark-submit --driver-memory=3g --conf 'spark.executor.memory=3g' --conf "spark.executor.extraJavaOptions=-XX:+UseG1GC" --conf "spark.mongodb.output.uri=mongodb://root:example@mongo/twitter.toto/?authSource=admin" --conf spark.cleaner.referenceTracking.cleanCheckpoints=true \
--driver-java-options "-Dlog4j.configuration=file:///var/code/python/batch_layer/log4j.properties" \
--master spark://spark2-master:7077 --deploy-mode client \
--packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.2,org.mongodb.spark:mongo-spark-connector_2.12:3.0.1  \
/var/code/python/speed_layer/window_streaming.py"

# correct
# tweeter feed
sudo docker run -it --rm --net docker_app-tier --name publish-tweets \
-v "/home/joseph/Documents/OpenClassroom/P5/dev/:/usr/src/dev" \
-e BEARER_TOKEN=$(cat ./auth/bearer_token.txt) \
docker_python python /usr/src/dev/python/publishers/get_sampled_stream.py

# speed layer consumer
sudo docker exec -e PYTHONIOENCODING=utf8 -it spark2-master \
/bin/bash -c "/opt/bitnami/spark/bin/spark-submit \
--driver-memory=1g \
--conf 'spark.executor.memory=1g' \
--conf "spark.executor.extraJavaOptions=-XX:+UseG1GC" \
--conf "spark.mongodb.output.uri=mongodb://root:example@mongo/twitter.toto/?authSource=admin" \
--conf spark.cleaner.referenceTracking.cleanCheckpoints=true \
--driver-java-options "-Dlog4j.configuration=file:///var/code/python/batch_layer/log4j.properties" \
--master local \
--packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.2,org.mongodb.spark:mongo-spark-connector_2.12:3.0.1  \
/var/code/python/speed_layer/window_streaming.py"

# spark://spark2-master:7077 \
# --deploy-mode client \

# batch layer consumer
sudo docker exec -e PYTHONIOENCODING=utf8 -it spark-master \
/bin/bash -c "/opt/bitnami/spark/bin/spark-submit \
--driver-memory=1g \
--conf 'spark.executor.memory=1g' \
--conf "spark.executor.extraJavaOptions=-XX:+UseG1GC" \
--conf spark.cleaner.referenceTracking.cleanCheckpoints=true \
--driver-java-options "-Dlog4j.configuration=file:///var/code/python/batch_layer/log4j.properties" \
--master local \
--packages org.apache.spark:spark-avro_2.12:3.0.2,org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.2  \
/var/code/python/batch_layer/stream_to_avro.py"

# spark://spark-master:7077 \
#--deploy-mode client \